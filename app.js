const Express = require("express");
const BodyParser = require("body-parser");

const app = Express();

app.use(Express.json());

app.use(BodyParser.urlencoded({ extended: true }));

// Mongoose.connect("mongodb://localhost/express", {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
// });

require('./app/routes/routes.js')(app);

app.listen(3000, () => {
  console.log("Listening at :3000...");
});




// const BookModel = Mongoose.model("book", {
//     title: String,
//     author: String,
//     published_date: Date,
//     pages: Number,
//     language: String,
//     Publisher_id: String
// });

// app.post("/books", async (request, response) => {
//   try {
//     var book = new BookModel(request.body);
//     var result = await book.save();
//     response.send(result);
//   } catch (error) {
//     response.status(500).send(error);
//   }
// });

// app.get("/books", async (request, response) => {
//   try {
//     var result = await BookModel.find().exec();
//     response.send(result);
//   } catch (error) {
//     response.status(500).send(error);
//   }
// });

// app.get("/books/:id", async (request, response) => {
//   try {
//     var book = await BookModel.findById(request.params.id).exec();
//     response.send(book);
//   } catch (error) {
//     response.status(500).send(error);
//   }
// });

// app.put("/books/:id", async (request, response) => {
//   try {
//     var book = await BookModel.findById(request.params.id).exec();
//     book.set(request.body);
//     var result = await book.save();
//     response.send(result);
//   } catch (error) {
//     response.status(500).send(error);
//   }
// });

// app.delete("/books/:id", async (request, response) => {
//   try {
//     var result = await BookModel.deleteOne({ _id: request.params.id }).exec();
//     response.send(result);
//   } catch (error) {
//     response.status(500).send(error);
//   }
// });