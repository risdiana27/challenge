const books = require('../models/book');
module.exports = (app) => {
    app.post("/books", async (request, response) => {
          try {
            var book = new books(request.body);
            var result = await book.save();
            response.send(result);
          } catch (error) {
            response.status(500).send(error);
          }
        });
        
        app.get("/books", async (request, response) => {
          try {
            var result = await books.find().exec();
            response.send(result);
          } catch (error) {
            response.status(500).send(error);
          }
        });
        
        app.get("/books/:id", async (request, response) => {
          try {
            var book = await books.findById(request.params.id).exec();
            response.send(book);
          } catch (error) {
            response.status(500).send(error);
          }
        });
        
        app.put("/books/:id", async (request, response) => {
          try {
            var book = await books.findById(request.params.id).exec();
            book.set(request.body);
            var result = await book.save();
            response.send(result);
          } catch (error) {
            response.status(500).send(error);
          }
        });
        
        app.delete("/books/:id", async (request, response) => {
          try {
            var result = await books.deleteOne({ _id: request.params.id }).exec();
            response.send(result);
          } catch (error) {
            response.status(500).send(error);
          }
        });

        app.use(function(req, res, next){
            return res.status(404).send({
                status: 404,
                message: "not found"
            });
        });

        app.use(function(err, req, res, nest){
            return res.status(505).send(err);
        });
};