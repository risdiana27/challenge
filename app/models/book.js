const Mongoose = require("mongoose");

Mongoose.connect("mongodb://localhost/challenge", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const books = Mongoose.model("book", {
    title: {
        type: String,
        minlength: [5, 'min 5 char'],
        required: true
    },
    author: {
        type: Array,
        required: true
    },
    published_date:{
        type: Date,
        required: true
    },
    pages:{
        type: Number,
        required: true
    },
    language: {
        type: String,
        minlength: [5, 'min 5 char'],
        required: true
    },
    publisher_id: {
        type: String,
        required: true
    }
});

module.exports = books